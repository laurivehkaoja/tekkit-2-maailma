# Tekkit 2 -maailma
Varmuuskopio Tekkit 2 -pelailuni maailmasta

# Käyttäminen
Jos haluat pelata samaa maailmaa, se onnistuu helposti.

Tekkit 2 -modipaketti pitää ensin asentaa koneelle. Paketin voi ladata esimerkiksi [Technic Launcherista](https://www.technicpack.net/) tai [PolyMC:stä](https://polymc.org/), joista suosittelen jälkimmäistä.

Kun paketti on asennettu, lataa tämä repositorio koneellesi joko zip-pakettina, tai kloonaa se suoraan Gitillä
```
git clone https://gitlab.com/laurivehkaoja/tekkit-2-maailma.git
```

Luo Tekkit 2:n `saves`-kansioon uusi kansio, joka on samalla maailmasi nimi. Siirrä tai pura Gitlab-repositorion sisältö tähän kansioon. Tämän jälkeen maailman pitäisi näkyä pelissä.